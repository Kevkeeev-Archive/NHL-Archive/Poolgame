var scene, camera, controls, renderer, clock, mouse, i, j;	// for ThreeJS
var strength, factor, friction;								// for Game
var pooltable, poolballs = [], cue, stick;					// Game Objects
var curPlayer, scores = [], players = [];					// Players

var debugLevel = 2;
function debug( text, level = -1 ) {
	if( level == -1 ){
		console.log("[TRACE] "+ text);
		console.trace();
	} else if( level <= debugLevel ){
		console.log(typeof text === "string" ? "[DBG] "+ text : ("[DBG] ", text) );
	}
}


/**
 * Handles when a key is pressed.
 */
function onDocumentKeyDown( event ) {
	event.preventDefault(); //prevent Ctrl+D bookmark, etc
	//keep gui usable (?)
	
	switch(event.code) {
		case "ShiftLeft": 
		case "ShiftRight":
		case "ControlLeft": 
		case "ControlRight":
			//ignore, used with A/D
		case "F12":
			//ignore, used for debug
			break; 
			
		case "KeyA": //move cue CW
			if(event.shiftKey && event.ctrlKey){
				cue.rotateY( (-Math.PI / 180) * 90);
				debug("KeyA: Moving cue CW * 90", 7);				
			} else if(event.shiftKey) {
				cue.rotateY( (-Math.PI / 180) * 10);
				debug("KeyA: Moving cue CW * 10", 7);
			} else if(event.ctrlKey) {
				cue.rotateY( (-Math.PI / 180) * 5);
				debug("KeyA: Moving cue CW * 5", 7);
			} else {
				cue.rotateY( (-Math.PI / 180) );
				debug("KeyA: Moving cue CW", 7);
			}
			break;

		case "KeyD": //move cue CCW
			if(event.shiftKey && event.ctrlKey){
				cue.rotateY( (Math.PI / 180) * 90);
				debug("KeyD: Moving cue CCW * 90", 7);				
			} else if(event.shiftKey) {
				cue.rotateY( (Math.PI / 180) * 10);
				debug("KeyD: Moving cue CCW * 10", 7);
			} else if(event.ctrlKey) {
				cue.rotateY( (Math.PI / 180) * 5);
				debug("KeyD: Moving cue CCW * 5", 7);
			} else {
				cue.rotateY( (Math.PI / 180) );
				debug("KeyD: Moving cue CCW", 7);
			}
			break;

		case "KeyS": //move cue back: more speed
			if(strength >= 0 && strength < 50) {
				cue.children[0].geometry.translate(0, -1, 0);
				strength +=1;
				debug("KeyS: Moving cue back", 7);
			} else {
				debug("KeyS: NOT Moving cue back (strength already at " + strength +")", 7);
			}
			break;

		case "KeyW": //move cue forward: less speed
			if(strength > 0 && strength <= 50) {
				cue.children[0].geometry.translate(0, 1, 0);
				strength -=1;
				debug("KeyW: Moving cue forward", 7);
			} else {
				debug("KeyW: NOT Moving cue forward (strength already at " + strength +")", 7);
			}
			break;

		case "Space": //shoot ball
			if(strength <= 0) {
				debug("Space: NOT Shooting (strength is at " + strength +")",7 );
			} else {
				var shootX, shootY, halfpi = Math.PI / 2;
				
				shootX = (cue.rotation.x == 0 ? 1.0 : -1.0) * ( ( halfpi - Math.abs(cue.rotation.y)) / halfpi );
				shootY = (cue.rotation.y / halfpi) * -1;
				
				debug("Space: Shooting (strength: " + strength + ", x: " + shootX + ", y: "+ shootY + ")", 4);
				poolballs[0].setSpeed( new THREE.Vector2( factor*strength*shootX, factor*strength*shootY ));

				cue.children[0].geometry.translate(0, strength, 0); strength = 0; //reset strength & cue;
				//cue.children[0] = setColourAndReturn(stick, 0x2a8a5d) ;
			}
			break;

		default:
			debug("[IGNO] Key pressed: " + event.key + " (code: " + event.code + "), but it has no special function.", 7);
			break;
	}
}

/**
 * handles when the window is resized, then resizes scene as well
 */
function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );
	render();
}

//poolballs[ poolballs.findIndex(findBallInArray, touchingBalls[j].number) ].reactToVector( reactSpeed );


/**
 * Function that fires whenever any ball touches another ball.
 * Will then handle the collision and any cascading collisions following from that.
 * 
 * Recursively: call (<any balls touching the starting[]>, <toIgnore + starting>)
 * @param starting Array of Poolballs, indicating the ball(s) that started the collission
 * @param toIgnore Array of Poolballs, that have been in a collision already 
 */
function handleCollision(/* PoolBall[] */ starting, /* PoolBall[] */ toIgnore) {
	debug("====================\nTesting collision for [ "+ starting.map( (ball) => ball.number +" ") +"] ![ "+ toIgnore.map( (ball) => ball.number + " " ) +"]", 5 );
	var collision = [];
	
	//get entire collision from starting balls
	for(var i=0; i<poolballs.length; i++){
		if( starting.indexOf( poolballs[i] ) < 0 && toIgnore.indexOf( poolballs[i] ) < 0 ) { 
			//Ball i is neither in starting[] or toIgnore[], so it's fair game to see if it collides with any ball in starting.
			starting.some( (startingBall) => {
				if( startingBall.isTouching(poolballs[i].ball.position) ) {
					collision.push(poolballs[i]);
					return true;
				}
			})
		}
	}
	if(collision.length <= 0) { debug("No collision!", 6); return; }
	debug("Found collision for [ "+ starting.map( (ball) => ball.number +" ") +"] ![ "+ toIgnore.map( (ball) => ball.number + " " ) +"] :[ "+ collision.map( (ball) => ball.number + " " ) +"]", 5);

	var collisionSpeed = new THREE.Vector2(0.0,0.0);
	collision.forEach( (collisionBall) => {
		collisionSpeed.x += collisionBall.speed.x;
		collisionSpeed.y += collisionBall.speed.y;
	})
	//collisionSpeed.x /= starting.length > 0 ? starting.length : 0;
	//collisionSpeed.y /= starting.length > 0 ? starting.length : 0;
	collisionSpeed = /* now an integer */ Math.sqrt( (collisionSpeed.x ** 2) + (collisionSpeed.y ** 2) );
	debug("Collision Speed: "+ collisionSpeed, 6);
	/** Speed FROM the collision balls, which goes EQUALLY to all starting balls. **/
	
	var startingSpeed = new THREE.Vector2(0.0,0.0);
	starting.forEach( (startingBall) => {
		startingSpeed.x += startingBall.speed.x;
		startingSpeed.y += startingBall.speed.y;
	});
	//startingSpeed.x /= collision.length > 0 ? collision.length : 0;
	//startingSpeed.y /= collision.length > 0 ? collision.length : 0;
	startingSpeed = /* now an integer */ Math.sqrt( (startingSpeed.x ** 2) + (startingSpeed.y ** 2) );
	debug("Starting Speed: "+ startingSpeed, 6);
	/** Speed FROM the starting balls, which goes EQUALLY to all collision balls. **/
	
	var location, touching;
	collision.forEach( (collisionBall) => {
		location = new THREE.Vector2( collisionBall.ball.position.x, collisionBall.ball.position.z );
		touching = 1;
		
		starting.forEach( (startingBall) => {
			if( startingBall.isTouching( collisionBall.ball.position ) ) {
				location.x += startingBall.ball.position.x;
				location.y += startingBall.ball.position.z;
				touching++;
			}
		})

		location.x = collisionBall.ball.position.x - (location.x / touching);
		location.y = collisionBall.ball.position.z - (location.y / touching);
		/** Now we have the location of the collision for this ball.**/
		
		/**
		 * With the (starting/collision)Speed, and the location{x,y}, we now have all three lengths of a Pythagorean triangle.
		 * Now, we have to translate that (int) Speed, to a Vector2 in the same direction as the Location vector.
		 */

		var smallSpeed = Math.sqrt( (location.x ** 2) + (location.y ** 2));
		var factor = startingSpeed / smallSpeed;
		collisionBall.speed.x = location.x * factor;
		collisionBall.speed.y = location.y * factor;
		
		if(isNaN( collisionBall.speed.x ) || collisionBall.speed.x == -0) { collisionBall.speed.x = 0.0;}
		if(isNaN( collisionBall.speed.y ) || collisionBall.speed.y == -0) { collisionBall.speed.y = 0.0;}
		debug("collisionBall["+ collisionBall.number +"] is touching: "+ (touching-1) + " balls @ "+ JSON.stringify(location) +" w/ speed: "+ JSON.stringify(collisionBall.speed), 5);
	});
	/** Fixed speed of all colliding balls **/
	
	starting.forEach( (startingBall) => {
		//do the same stuff as collision.forEach() above.
		location = new THREE.Vector2( startingBall.ball.position.x, startingBall.ball.position.z );
		touching = 1;
		
		collision.forEach( (collisionBall) => {
			if( collisionBall.isTouching( startingBall.ball.position ) ) {
				location.x += collisionBall.ball.position.x;
				location.y += collisionBall.ball.position.z;
				touching++;
			}
		})
		location.x = startingBall.ball.position.x - (location.x / touching);
		location.y = startingBall.ball.position.z - (location.y / touching);
		/** Now we have the location of the collision for this ball.**/
		
		/**
		 * With the (starting/collision)Speed, and the location{x,y}, we now have all three lengths of a Pythagorean triangle.
		 * Now, we have to translate that (int) Speed, to a Vector2 in the same direction as the Location vector.
		 */
		
		var smallSpeed = Math.sqrt( (location.x ** 2) + (location.y ** 2));
		var factor = collisionSpeed / smallSpeed;
		startingBall.speed.x = location.x * factor;
		startingBall.speed.y = location.y * factor;

		if(isNaN( startingBall.speed.x ) || startingBall.speed.x == -0) { startingBall.speed.x = 0.0;}
		if(isNaN( startingBall.speed.y ) || startingBall.speed.x == -0) { startingBall.speed.y = 0.0;}
		debug("startingBall["+ startingBall.number +"] is touching: "+ (touching-1) + " balls @ "+ JSON.stringify(location) +" w/ speed: "+ JSON.stringify(startingBall.speed), 5);

	})
	/** Fixed speed of all starting balls **/
	
	var newIgnore = toIgnore.concat(starting);
	debug("Starting: [ "+ starting.map( (ball) => ball.number +" ") +"]\ncollision: [ "+ collision.map( (ball) => ball.number + " " ) +"]\ntoIgnore: [ "+ toIgnore.map( (ball) => ball.number + " " ) +"]", 5);
	debug("New run> collision: [ "+ collision.map( (ball) => ball.number +" ") +"]\ntoIgnore: [ "+ newIgnore.map( (ball) => ball.number + " " ) +"]", 5);
	
	//TODO fix recursive!
	handleCollision ( //oh the joys of passing by reference, where you don't have to worry about pointers because everything points to the same item.
		collision,
		newIgnore //toIgnore.concat(starting)
	); //recursively get entire collision from previous collision, excluding all balls IN previous collisionS
	
}

function render() {
    requestAnimationFrame(render);
    
    var isRolling = false; //assume all balls are not rolling.
    /** First: move all balls **/
    for(i = 0; i < poolballs.length; i++) {
    	if(poolballs[i].isRolling()) {
	    	isRolling = true;
    		poolballs[i].move( friction ); //moves, does hole and wall collisions
    		handleCollision([ poolballs[i] ], [] ); //does all ball collisions
    	}
    }
    
	/** Then: fix the cue **/
    if(isRolling) {
    	cue.visible = false; // hide the cue as long as balls are rolling.
    } else {
    	if(! poolballs[0].ball.visible ){ // if white ball was putted, hide it outside the area invisible, and now reset it
    		//reset ball
    		poolballs[0].ball.visible = true; 
    		poolballs[0].ball.position.x = -58.5; 
    		poolballs[0].ball.position.z = 0; 

    		//reset camera
    		camera.position.x = -150;
    		camera.position.y = 120;
    		camera.position.z = 0;
    		
    		//reset cue
    		cue.rotation.y = 0;
    		if(cue.rotation.x != 0){
    			cue.rotation.y = Math.PI;
    		}
    	}
    	if(! cue.visible ) { //switch players after the white ball has stopped rolling.

    		//cue.children[0] = stick.setColourAndReturn(0x202020); //testcolour
    		handlePoolEvent( new PoolEvent( "switchPlayer", 0 ) );

    		cue.visible = true;  // and show it again to take another shot.
    	}

    	controls.target.copy(poolballs[0].ball.position);// update camera
    	cue.position.copy( poolballs[0].ball.position ); // update cue position.
    	//TODO nice: cue.rotation = controls.rotation?
    }
    controls.update();
    //based on if balls are rolling, switch view to table (without cue) or the cueball (with cue)
    
    scene.updateMatrix();
    renderer.render(scene,camera);
}

function init () {
	renderer = new THREE.WebGLRenderer();
	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.setClearColor(0xcee0ff);
	document.body.appendChild(renderer.domElement);
	
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
	controls = new THREE.OrbitControls( camera, renderer.domElement );
	
	clock = new THREE.Clock();
	mouse = new THREE.Vector2();

	//Lighting
	scene.add( new PoolLight() );
	
	//Table
	pooltable = new PoolTable();
	scene.add(pooltable);
	
	camera.position.x = -150;
	camera.position.y = 120;
	camera.lookAt(pooltable.position); 
	controls.update();

	//Balls
	var poolball;
	for(i = 0; i<=15; i++) {
		poolball = new PoolBall(i);	
		scene.add(poolball.ball);
		poolballs.push(poolball);
	}
	friction = 0.995;
	
	//Cue
	stick = new PoolStick();
	cue = new THREE.Object3D; //acts as pivot for model
	cue.add( setColourAndReturn(stick, 0x8b5a2b) );
	cue.position.copy(poolballs[0].ball.position);
	scene.add(cue);
	strength=0; factor=10; //cue shooting factor

	//Players
	curPlayer = 0;
	
	document.addEventListener( 'keydown'  , onDocumentKeyDown );
	  window.addEventListener( 'resize'   , onWindowResize, false);
	  
	render(); //enter the render loop.
}

function handlePoolEvent(/* PoolEvent */ e){
	//TODO handle event.type event.msg
	
	debug(e, 1);
	switch(e.type){
		case "pocket":
			debug("[POCK] Ball " + e.msg + " is pocketed by " +  players[curPlayer].name + ".", 1);
			poolballs[ poolballs.findIndex(findBallInArray, e.msg) ].ball.visible = false; //hide from scene
			poolballs.splice( poolballs.findIndex(findBallInArray, e.msg), 1); //remove from poolballs
			
			players[ curPlayer ].pocketBall( e.msg );
			updatePlayer( players[curPlayer] )
			
			break;
		case "switchPlayer":
			curPlayer = 1 - curPlayer; //switch index
			players[ curPlayer ].turn++;
			updatePlayer( players[curPlayer] );
			debug("[SWIT] Switched to player "+ curPlayer, 1);
			break;
	}
	
	
}

/**
 * Emergency button to freeze all balls directly.
 */
function stop(){
	poolballs.forEach( (ball) => {
		ball.speed.x = 0.0;
		ball.speed.y = 0.0;
	})
}

/** 
 * Help function: poolballs.findIndex( findBallInArray, this ) will return index of the given poolball.
 * @param ball current ball to compare
 * @param this = required ballnumber
 * @returns true if correct ball, otherwise false.
 */
function findBallInArray(ball){
	return ball.number == this;
}


init();
$( document).ready( () => { 
	//page is loaded, fire init() function.

	for(var i=0; i<2; i++){
		var name = prompt("Player "+ i +"'s name:");
		if(name == null || name == ""){ name = i ? "Sjakie" : "Sjonnie"; }
		players.push( new PoolPlayer (name, i%2 == 0) );
	} //prepare players
	updatePlayer( players[curPlayer] );

});
