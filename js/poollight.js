class PoolLight {

	constructor (){
		var barLighting = new THREE.Group();
		//define lights
		
		// white spotlight shining from the side, casting a shadow
		var spotLight = new THREE.SpotLight( 0xffffd5, 0.7 );
		spotLight.castShadow = true;
		
		spotLight.shadow.mapSize.width = 1024;
		spotLight.shadow.mapSize.height = 1024;
		
		spotLight.shadow.camera.near = 500;
		spotLight.shadow.camera.far = 4000;
		spotLight.shadow.camera.fov = 30;
	
		//spotlight top left:
		spotLight.position.set( 1000, 1500, 0 );
		barLighting.add( spotLight );
	
		
		//spotlight top right
		var spotLight2 = spotLight.clone();
		spotLight2.position.set(-1000, 1500, 0);
		barLighting.add(spotLight2);
		
		//helplight for bottom of table
		var spotLight3 = spotLight.clone();
		spotLight3.position.set( 0, -150, 0 );
		barLighting.add(spotLight3);
		
		return barLighting;
	}

}